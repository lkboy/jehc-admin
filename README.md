# ** **QQ群：102642604** **
# jehc-admin（本工程为maven单工程版本）
JEHC开源平台，采用技术为：spring，springMVC，Mybatis，Activiti5，（Activiti可视化设计器基于IE，火狐，谷歌，360等浏览器），Solr4.10，Mysql，Redis，Ehcache，服务器监控模块，tk压缩，Extjs6.2 ，BootStrap，Junit单元测试，Logback，同时融入了Hessian，数据库读写分离，MQ消息中间件等技术


统一开发 统一开放 统一规范 统一开源
https://gitee.com/jehc/jehc-admin

 **开发工具：** 
eclipse-jee-mars-1
或eclipse-jee-mars-2
或eclipse-juno
或STS
或IDEA


maven版本地址:
maven多工程依赖版本如下地址：

https://gitee.com/jehc/jehc

非maven版本地址如下：
https://gitee.com/jehc/jehc-none-maven

效果图片如下：

jEhc能做什么
![输入图片说明](https://gitee.com/uploads/images/2018/0314/104427_503cc218_1341290.png "首页默认.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0314/104443_fa41c016_1341290.png "首页黑白.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0314/104459_9b5ee8bb_1341290.png "在线设计.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0314/104517_5ff16127_1341290.png "流程管理.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0314/104530_b3b7baf7_1341290.png "角色权限.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0314/104543_3c0b3cd8_1341290.png "分配用户.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0314/104554_86be4e3a_1341290.png "用户体系.png")

只列举小部分功能如下：

![输入图片说明](https://gitee.com/uploads/images/2018/0314/104411_56e6d687_1341290.png "各种功能.png")

电商后端及前端
![输入图片说明](https://gitee.com/uploads/images/2018/0214/221719_48b84487_1341290.png "购物车.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0214/221728_30787402_1341290.png "订单.png")
统一开发 统一开放 统一规范 统一开源
https://gitee.com/jehc/jehc-none-maven/

 **开发工具：** 
eclipse-jee-mars-1
或eclipse-jee-mars-2
或eclipse-juno
或STS
或IDEA

maven版本地址:
maven多工程依赖版本如下地址：

https://gitee.com/jehc/jehc

非maven版本地址如下：
https://gitee.com/jehc/jehc-none-maven

### 开发前部署老出现问题 
一般出现环境问题（主要是编译问题较多）
建议如下操作
一、
![输入图片说明](https://git.oschina.net/uploads/images/2017/0903/121008_95886692_1341290.png "clean.png")
